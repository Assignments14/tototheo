<?php

declare(strict_types=1);

function final_output(string $output): string {
    $challenge_token = 'a6jb7wt3852';
    $challenge_token_arr = str_split($challenge_token);

    $final_output = '';

    foreach (str_split($output) as $c) {
        if (in_array($c, $challenge_token_arr, true)) {
        $c = "--$c--";
        }
        $final_output .= $c;
    }

    return $final_output;
}

function StringChallenge($str) {

  $challenge_token = 'a6jb7wt3852';
  $challenge_token_arr = str_split($challenge_token);

  $chars = str_split($str);

  $output = '';
  $curr_char_counts = 1;
  $curr_char = array_shift($chars);

  foreach ($chars as $c) {
    if ($c === $curr_char) {
      $curr_char_counts++;
      continue;
    }

    $output .= $curr_char_counts . $curr_char;

    $curr_char = $c;
    $curr_char_counts = 1;
  }
  $output .= $curr_char_counts . $curr_char;

  return final_output($output); 
}
   
// keep this function call here  
assert(StringChallenge("aabbcde") === '--2----a----2----b--1c1d1e', "Test Failed");
assert(StringChallenge("wwwbbbw") === '--3----w----3----b--1--w--', "Test Failed");