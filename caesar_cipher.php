<?php

declare(strict_types=1);

function StringChallenge(string $str, int $num): string {
    $output = '';

    foreach (str_split($str) as $c) {
        if (ctype_punct($c)) {
            $output .= $c;
            continue;
        }
        $output .= chr(ord($c) + $num);
    }

    $challenge_token = 'a6jb7wt3852';
    $challenge_token_arr = str_split($challenge_token);

    $final_output = '';

    foreach (str_split($output) as $c) {
        if (in_array($c, $challenge_token_arr, true)) {
          $c = "--$c--";
        }
        $final_output .= $c;
    }

    return $final_output;
}

assert(StringChallenge('Hello', 4) === 'Lipps', 'Test Failed');
assert(StringChallenge('abc', 0) === '--a----b--c', 'Test Failed');