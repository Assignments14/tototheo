<?php

declare(strict_types=1);

ini_set('assert.exception', false);
ini_set('assert.warning', true);

/**
 * Have the function StringChallenge(str) read str which will contain two strings separated by a space.
 * 
 * The first string will consist of the following sets of characters: +, *, and {N} which is optional. 
 * 
 * The plus (+) character represents a single alphabetic character, 
 * the asterisk (*) represents a sequence of the same character of length 3 
 * unless it is followed by {N} which represents how many characters 
 * should appear in the sequence where N will be at least 1. 
 * 
 * Your goal is to determine if the second string exactly matches the pattern of the first string in the input.

  For example: 
  if str is "++*{5} gheeeee" then the second string in this case does match the pattern, 
  so your program should return the string true. 
  If the second string does not match the pattern your program should return the string false.

  Input: "+++++* abcdemmmmmm"
  Output: false

  Input: "**+*{2} mmmrrrkbb"
  Output: true

 */

 function StringChallenge($str): string {

    [$pattern, $subject] = explode(' ', $str);

    $track = [];
    $matched = false;

    $len = strlen($subject);

    $repeat_count = 3;
    if (($b = strpos($pattern, '{')) !== false) {
        $repeat_count = (int) substr($pattern, $b+1, 1);
    }

    $i = 0;
    foreach (str_split($pattern) as $m) {
        if ($m === '{') {
            break;
        }
        if ($m === '+') {
            $i++;
            continue;
        }
        if ($m === '*') {
            $c = $subject[$i];
            $track[$c] = 1;
            $i++;
            while ($i < $len && ($subject[$i] === $c)) {
                $track[$c]++;
                $i++;
            }
            if ($track[$c] === $repeat_count) {
                $matched = true;
            }
        }
    }
    return $matched ? 'true' : 'false';

}

assert(StringChallenge('++*{5} gheeeee') === 'true', 'Test Failed');
assert(StringChallenge('+++++* abcdemmmmmm') === 'false', 'Test Failed');
assert(StringChallenge('**+*{2} mmmrrrkbb') === 'true', 'Test Failed');